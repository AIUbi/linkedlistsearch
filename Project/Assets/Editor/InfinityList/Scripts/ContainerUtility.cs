﻿using System;
using System.Reflection;
using UnityEngine;
/*
* THOUGHTS:
* I don't know the size of container and it filled with random values, but I know that any container always has limited size
* So minimumContainerSize always must be equal to minimal possible length of a Container.
* What I can do with this:
* 1. With reflection to access private members of Container. (hmmmmmmm, that means O(1) by memory and CPU)
* 2. With unsafe directive to work with pointers. (impossible because of getters)
* 3. What if the value of a node would be nullable bool? Value; That would be better.
* 4. Algorithm for task by using existing methods.
* Actually is a bit impossible to find EXACT length because it is error prone due search window size is could be smaller or greater than count of Nodes.
* so it requires a loop detection and so on.
* But how about detection of values with some pattern?
* Pattern with modulo of 2 - value = Index % 2 == 0; It's not an option because random could generate a sequence of true false true false true etc.
* Pattern with Fibonacci Each value that multiple of two should be as False else True; Would work, could be modified: to detect a sequence of False or True, each number of fibonacci sequence
* represents a length of a sequence of True and False(True if modulo of 2 == 0), but again, when the algorithm should stop?
* I have two methods - first one MoveForward, second one MoveBackward so what if while moving Forward all values will be set to true and moving Backward to false? That should work.
* Sounds good but again that didn't work because at each iteration before setting value to true or false I need to check that value is inverted or not. For example:
* MoveForward() current == true, MoveBackward() current == false;
* Anyway I'll use brute-force to calculate the amount of elements in a simplest way (trying to avoid inefficient memory allocation, because I always has choose between CPU time or RAM amount C: )
*/

namespace Assets.Editor.InfinityList.Scripts
{
    //Utility class to calculate count of elements in Container.
    public static class ContainerUtility
    {
        /*
         * container - object that represents a double linked list
         * return - count of elements in Container
         */
        public static int CountElements(Container container)
        {
            //Remember previous element from Start
            container.MoveBackward();
            bool lastValue = container.Value;
            
            //The "Start" of List
            int currentIndex = 0;

            while (true)
            {
                //Move forward at one step.
                container.MoveForward();
                currentIndex++;

                //Saving value of current element to restore it later.
                bool currentValue = container.Value;
                //Set it to inverted lastValue to detect changes
                container.Value = !lastValue;
                //Moving to start and check the value is changed or not and then move forward
                if (IsLastChanged(container, currentIndex, lastValue))
                {
                    //Restoring the value on checked index
                    container.Value = currentValue;
                    container.MoveForward();

                    return currentIndex;
                }

                //Restoring the value on checked index
                container.Value = currentValue;
            }
        }

        /*
         * container - object that represents a intrusive double linked list
         * moveStepCount - how much we need to go back to achieve starting position
         * originalValue - the boolean value of latest node.
         * return - count of elements in Container
         */
        private static bool IsLastChanged(Container container, int moveStepCount, bool originalValue)
        {
            bool isChanged = false;

            for(int I = 0; I < moveStepCount; ++I)
            {
                container.MoveBackward();
            }

            isChanged = container.Value != originalValue;

            for (int I = 0; I < moveStepCount; ++I)
            {
                container.MoveForward();
            }

            return isChanged;
        }

        /*
        public static int GetRandomizedCount(ref Container container)
        {
            return (int)typeof(Container).GetField("count", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(container);
        }
        */
    }
}
