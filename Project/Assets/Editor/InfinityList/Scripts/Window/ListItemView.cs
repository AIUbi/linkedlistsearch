﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.InfinityList.Scripts.Window
{
    public sealed class ListItemView
    {
        private GUIStyle _style;
        public bool Value; //In more complex cases should be a model with read only access.

        public ListItemView(bool value)
        {
            _style = new GUIStyle();
            _style.fixedHeight = 128;
            _style.fontStyle = FontStyle.Bold;
            _style.alignment = TextAnchor.MiddleCenter;
            _style.normal.background = Texture2D.whiteTexture;
            _style.margin = new RectOffset(4, 4, 4, 4);
            Value = value;
        }

        public void OnGUI()
        {
            GUI.backgroundColor = Value ? Color.green * 0.5f : Color.red * 0.5f;
            GUILayout.Label(Value.ToString(),_style);
        }
    }
}
