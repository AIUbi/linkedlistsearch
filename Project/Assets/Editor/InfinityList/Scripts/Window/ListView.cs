﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.InfinityList.Scripts.Window
{
    //All commented parts of code - smooth scrolling with bugs, I'm not familiar with IMGUI.
    //Have thinking about MVC model.
    public sealed class ListView
    {
        private Vector2 _scrollPosition;
        private Vector2 _targetScrollPosition;
        private int _countOfElementsInScrollView = 6; //Should be calculated with GUILayoutUtility.GetLastRect()

        private List<ListItemView> _views;
        private List<bool> _values;

        public ListView(Container container)
        {
            _views = new List<ListItemView>();
            _values = new List<bool>();
            
            Initialize(container);
        }

        private void Initialize(Container container)
        {
            int count = ContainerUtility.CountElements(container);

            for (int I = 0; I < count; ++I)
            {
                _values.Add(container.Value);
                container.MoveForward();
            }

            for (int I = 0; I < _countOfElementsInScrollView; ++I)
            {
                _views.Add(new ListItemView(_values[I % count]));
            }
        }
      
        public void OnGUI()
        {
            if (_views.Count == 0) return;

            GUILayout.BeginVertical();

            for (int I = 0; I < _views.Count; ++I)
            {
                _views[I].OnGUI();
            }

            GUILayout.EndVertical();
        }

        public void UpdateScroll()
        {
            float delta = Event.current.delta.y;

            if (delta < 0)
            {
                _views.Add(_views[0]);
                _views.RemoveAt(0);
            }

            if (delta > 0)
            {
                _views.Insert(0, _views[_views.Count - 1]);
                _views.RemoveAt(_views.Count - 1);
            }
        }
    }
}
