﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;
namespace Assets.Editor.InfinityList.Scripts.Window
{
    //All commented parts of code - smooth scrolling with bugs, I'm not familiar with IMGUI.
    public class InfinityListWindow : EditorWindow
    {
        ListView _listView;
        Container _container;
        int _sizeOfContainer = 0;

        [MenuItem("Window/LinkedListSearch")]
        static void Init()
        {
            InfinityListWindow listWindow = (InfinityListWindow)GetWindow(typeof(InfinityListWindow));
            listWindow.Show();
        }

        void Initialize()
        {
            _container = new Container();
            _sizeOfContainer = ContainerUtility.CountElements(_container);
            _listView = new ListView(_container);
        }

        private void Update()
        {
            if (_container == null) Initialize();
        }

        void OnGUI()
        {
            if (_listView == null) return;

            GUILayout.BeginVertical();

            GUILayout.Label("Infinity list", EditorStyles.boldLabel);
            GUILayout.Label("Unique node count:"+_sizeOfContainer, EditorStyles.boldLabel);

            if (GUILayout.Button("Create new"))
            {
                Initialize();
            }

            GUILayout.EndVertical();

            _listView.OnGUI();

            if (Event.current.type == EventType.ScrollWheel)
            {
                _listView.UpdateScroll();
                Repaint();
            }
        }
    }

}
