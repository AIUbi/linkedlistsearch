﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Assets.Editor.InfinityList.Scripts;
namespace Tests
{
    public class ContainerTests
    {
        [Test]
        public void CountEqual16()
        {
            Container container = new Container(16);

            Assert.AreEqual(16, ContainerUtility.CountElements(container));
        }

        [Test]
        public void CountEqual256()
        {
            Container container = new Container(256);

            Assert.AreEqual(256, ContainerUtility.CountElements(container));
        }

        [Test]
        public void CountGreaterThanZero()
        {
            Container container = new Container(-1);

            Assert.IsTrue(ContainerUtility.CountElements(container) > 0);
        }

        [Test]
        public void DataEqualityAfterCounting()
        {
            Container container = new Container(10);
            List<bool> data = new List<bool>();

            for(int I = 0; I < 10; ++I)
            {
                data.Add(container.Value);
                container.MoveForward();
            }

            ContainerUtility.CountElements(container);

            for (int I = 0; I < 10; ++I)
            {
                Assert.IsTrue(container.Value == data[I]);
                container.MoveForward();
            }
        }
    }
}
